/*
  Simple sampling 2d Ising model
  Date: 14 Sep 2016
  Author: Sachin Krishnan T V (sachin@physics.iitm.ac.in)
*/

#include <iostream>
#include <fstream>
#include <random>
#include <cstring>
#include "pcg_random.hpp"

#define N 256  // Size of the system
#define MCS 1000  // Number of independent configurations
#define INT 1.5 // Magnitude of interaction potential
#define EXT 0.0 // Magnitude of external field

using namespace std;

int main()
{
  int i, j, k;
  int il, ir;
  int jl, jr;
  
  // This captures the configuration. false -> down, true -> up.
  bool lattice[N][N]; 
  // char filename[256];
  ofstream dumpf;
  ofstream logf;
  
  pcg_extras::seed_seq_from<random_device> seed_source;
  pcg32 rng(seed_source);
  uniform_int_distribution<int> udist(0, 1);

  double energy;
  double mag_per_step;

  logf.open("log.dat", ios::out);
  
  // Initial state
  for(i = 0; i < N;i++)
    for(j = 0;j < N;j++)
      lattice[i][j] = false;
  
  
  // Main MC Loop
  for(k = 0; k < MCS;k++)
    {
      for(i = 0;i < N;i++)
	for(j = 0;j < N;j++)
	  lattice[i][j] = static_cast<bool>(udist(rng));

      energy = 0.0;
      mag_per_step = 0.0;
      for(i = 0; i < N;i++)
	{
	  il = (i==0)?N-1:i-1;
	  ir = (i==N-1)?0:i+1;
	  for(j = 0; j < N;j++)
	    {
	      jl = (j==0)?N-1:i-1;
	      jr = (j==N-1)?0:i+1;
	  
	      energy += ((lattice[i][j] ^ lattice[il][j])?INT:-INT);
	      energy += ((lattice[i][j] ^ lattice[ir][j])?INT:-INT);
	      energy += ((lattice[i][j] ^ lattice[i][jl])?INT:-INT);
	      energy += ((lattice[i][j] ^ lattice[i][jr])?INT:-INT);

	      energy += -EXT * ((lattice[i][j])?1:-1);

	      mag_per_step += ((lattice[i][j])?1:-1);
	    }
	}
      mag_per_step /= (static_cast<double>(N*N));
      logf<<k<<"\t"<<energy<<"\t"<<mag_per_step<<endl;
      
      // Output configuration
      /*sprintf(filename, "dump_%d.dat", k);
      dumpf.open(filename,ios::out);
      for(i = 0; i < N;i++)
	for(j = 0;j < N;j++)
	  if(lattice[i][j])
	    dumpf<<i<<"\t"<<j<<endl;
      dumpf.close();
      */
    }
  
  logf.close();
  return 0;
}
